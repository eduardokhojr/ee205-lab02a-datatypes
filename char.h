///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file char.h
/// @version 1.0
///
/// Print the characteristics of the "char", "signed char" and "unsigned char" datatypes.
///
/// @author Eduardo Kho Jr <eduardok@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   1/21/2021
///////////////////////////////////////////////////////////////////////////////

extern void doChar();            /// Print the characteristics of the "char" datatype
extern void flowChar();          /// Print the overflow/underflow characteristics of the "char" datatype

extern void doSignedChar();      /// Print the characteristics of the "signed char" datatype
extern void flowSignedChar();    /// Print the overflow/underflow characteristics of the "signed char" datatype

extern void doUnsignedChar();    /// Print the characteristics of the "unsigned char" datatype
extern void flowUnsignedChar();  /// Print the overflow/underflow characteristics of the "unsigned char" datatype

