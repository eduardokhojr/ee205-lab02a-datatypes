///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file int.c
/// @version 1.0
///
/// Print the characteristics of the "int" and "unsigned int" datatypes.
///
/// @author Eduardo Kho Jr <eduardok@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   1/21/2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "int.h"


///////////////////////////////////////////////////////////////////////////////
/// int

/// Print the characteristics of the "int" datatype
void doInt() {
   printf(TABLE_FORMAT_INT, "int", sizeof(int)*8, sizeof(int), INT_MIN, INT_MAX);
}


/// Print the overflow/underflow characteristics of the "int" datatype
void flowInt() {
 int overflow = INT_MAX;
 printf("int overflow:  %d + 1 ", overflow++);
 printf("becomes %d\n", overflow);

 int underflow = INT_MIN;
 printf("int underflow:  %d - 1 ", underflow--);
 printf("becomes %d\n",underflow);
}


///////////////////////////////////////////////////////////////////////////////
/// unsigned int

/// Print the characteristics of the "unsigned int" datatype
void doUnsignedint() {
   printf(TABLE_FORMAT_UINT, "unsigned int",sizeof(unsigned int)*8, sizeof(unsigned int), 0, UINT_MAX);
}

/// Print the overflow/underflow characteristics of the "unsigned int" datatype
void flowUnsignedint() {
 long overflow = UINT_MAX;
 printf("unsigned int overflow:  %ld + 1", overflow++);
 printf(" becomes %d\n", 0);

 printf("unsigned int underflow:  %d - 1",0);
 printf(" becomes %ld\n", overflow--);
}

