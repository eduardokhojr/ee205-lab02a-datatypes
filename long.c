///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file long.c
/// @version 1.0
///
/// Print the characteristics of the "long" and "unsigned long" datatypes.
///
/// @author Eduardo Kho Jr <eduardok@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   1/21/2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "long.h"


///////////////////////////////////////////////////////////////////////////////
/// long

/// Print the characteristics of the "long" datatype
void doLong() {
   printf(TABLE_FORMAT_LONG, "long", sizeof(long)*8, sizeof(long), LONG_MIN, LONG_MAX);
}


/// Print the overflow/underflow characteristics of the "long" datatype
void flowLong() {
 long overflow = LONG_MAX;
 printf("long overflow:  %ld + 1 ", overflow++);
 printf("becomes %ld\n", overflow);

 long underflow = LONG_MIN;
 printf("long underflow:  %ld - 1 ", underflow--);
 printf("becomes %ld\n",underflow);
}


///////////////////////////////////////////////////////////////////////////////
/// unsigned long

/// Print the characteristics of the "unsigned long" datatype
void doUnsignedlong() {
   printf(TABLE_FORMAT_ULONG, "unsigned long",sizeof(unsigned long)*8,  sizeof(unsigned long), 0, ULONG_MAX);
}
/// Print the overflow/underflow characteristics of the "unsigned long" datatype
void flowUnsignedlong() {
 long double overflow = ULONG_MAX;
 printf("unsigned long overflow:  %0.0Lf + 1", overflow);
 printf(" becomes %d\n", 0);

 printf("unsigned long underflow:  %d - 1",0);
 printf(" becomes %0.0Lf\n", overflow);
}

